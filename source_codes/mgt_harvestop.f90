      subroutine mgt_harvestop (jj, iplant, iharvop)

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine performs the harvest operation (no kill)

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units          |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    curyr       |none           |current year in simulation
!!    harveff       |none         |harvest efficiency: fraction of harvested 
!!                                |yield that is removed from HRU; the 
!!                                |remainder becomes residue on the soil
!!                                |surface
!!    hvsti(:)    |(kg/ha)/(kg/ha)|harvest index: crop yield/aboveground
!!                                |biomass
!!    ihru        |none           |HRU number
!!    wsyf(:)     |(kg/ha)/(kg/ha)|Value of harvest index between 0 and HVSTI
!!                                |which represents the lowest value expected
!!                                |due to water stress
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units          |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    rsr1c(:)    |               |initial root to shoot ratio at beg of growing season
!!    rsr2c(:)    |               |root to shoot ratio at end of growing season
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units          |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    xx          |
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp, Min

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use basin_module
      use organic_mineral_mass_module
      use hru_module, only : hru, ihru
      use soil_module
      use plant_module
      use plant_data_module
      use mgt_operations_module
      use constituent_mass_module
      use carbon_module
      
      implicit none
     
      integer :: j                     !none           |HRU number
      integer :: k                     !none           |counter
      integer :: idp                   !               |
      integer, intent (in) :: jj       !none           |counter
      integer, intent (in) :: iplant   !               |plant number xwalked from hlt_db()%plant and plants.plt
      integer, intent (in) :: iharvop  !               |harvest operation type
      integer :: ipl                   !none           |counter
      integer :: orgc_f                !fraction       |fraction of organic carbon in fertilizer
      integer :: l                     !               |
      integer :: icmd                  !               |
      real :: resnew                   !               |
      real :: resnew_n                 !               |
      real :: hiad1                    !none           |actual harvest index (adj for water/growth)
      real :: wur                      !none           |water deficiency factor
      real :: clip                     !kg/ha          |yield lost during harvesting
      real :: clipn                    !kg N/ha        |nitrogen in clippings
      real :: clipp                    !kg P/ha        |phosphorus in clippings
      real :: clippst                  !kg pst/ha      |pesticide in clippings
      real :: yieldn                   !               |
      real :: yieldp                   !               |
      real :: yldpst                   !kg pst/ha      |pesticide removed in yield
      real :: rtfr                     !none           |root fraction
      real :: rtres                    !               | 
      real :: rtresn                   !               |           
      real :: rtresp                   !               | 
      real :: abgr_remove_fr           !               |fraction  of above ground biomass lost at harvest
      real :: ff4                      !               | 
      real :: xx                       !none           |variable to hold calculation value  
      real :: yield                    !kg             |yield (dry weight)
      real :: hi_ovr                   !!kg/ha)/(kg/ha)|harvest index target specified at harvest
      real :: harveff

      j = jj
      ipl = iplant
            
      idp = pcom(j)%plcur(ipl)%idplt
      hi_ovr = harvop_db(iharvop)%hi_ovr
      harveff = harvop_db(iharvop)%eff

      hiad1 = 0.
      if (hi_ovr > 0.) then
        hiad1 = hi_ovr
      else
        if (pcom(j)%plg(ipl)%plet < 10.) then
          wur = 100.
        else
          wur = 100. * pcom(j)%plg(ipl)%plet / pcom(j)%plg(ipl)%plet
        endif
        hiad1 = (pcom(j)%plg(ipl)%hvstiadj - pldb(idp)%wsyf) *          &   
            (wur / (wur + Exp(6.13 - .0883 * wur))) + pldb(idp)%wsyf
        if (hiad1 > pldb(idp)%hvsti) then
          hiad1 = pldb(idp)%hvsti
        end if
      end if

!! check if yield is from above or below ground
      if (pldb(idp)%hvsti > 1.001) then
        yield = pl_mass(j)%tot(ipl)%m * (1. - 1. / (1. + hiad1))
      else
        yield = (1. - pcom(j)%plg(ipl)%root_frac) * pl_mass(j)%tot(ipl)%m * hiad1
      endif
      if (yield < 0.) yield = 0.

!! determine clippings (biomass left behind) and update yield
      clip = yield * (1. - harveff)
      yield = yield * harveff
      if (yield < 0.) yield = 0.
      if (clip < 0.) clip = 0.

      if (hi_ovr > 0.) then
        !! calculate nutrients removed with yield
        yieldn = yield * pcom(j)%plm(ipl)%n_fr
        yieldp = yield * pcom(j)%plm(ipl)%p_fr
        yieldn = Min(yieldn, 0.80 * pl_mass(j)%tot(ipl)%n)    ! note Armen changed .80 for 0.9
        yieldp = Min(yieldp, 0.80 * pl_mass(j)%tot(ipl)%p) ! note Armen changed .80 for 0.9
        !! calculate nutrients removed with clippings
        clipn = clip * pcom(j)%plm(ipl)%n_fr
        clipp = clip * pcom(j)%plm(ipl)%p_fr
        clipn = Min(clipn, pl_mass(j)%tot(ipl)%n - yieldn)
        clipp = Min(clipp, pl_mass(j)%tot(ipl)%p - yieldp)
      else
        !! calculate nutrients removed with yield
        yieldn = yield * pldb(idp)%cnyld
        yieldp = yield * pldb(idp)%cpyld
        yieldn = Min(yieldn, 0.80 * pl_mass(j)%tot(ipl)%n) ! note Armen changed .80 for 0.9
        yieldp = Min(yieldp, 0.80 * pl_mass(j)%tot(ipl)%p) ! note Armen changed .80 for 0.9
        !! calculate nutrients removed with clippings
        clipn = clip * pldb(idp)%cnyld
        clipp = clip * pldb(idp)%cpyld
        clipn = Min(clipn, pl_mass(j)%tot(ipl)%n - yieldn)
        clipp = Min(clipp, pl_mass(j)%tot(ipl)%p - yieldp)
      endif

      yieldn = Max(yieldn,0.)
      yieldp = Max(yieldp,0.)
      clipn = Max(clipn,0.)
      clipp = Max(clipp,0.)
      
      resnew = clip 
      resnew_n = clipn
      call pl_leaf_drop (resnew, resnew_n)

	!! Calculation for dead roots allocations, resetting phenology, updating other pools
      if (pl_mass(j)%ab_gr(ipl)%m > 1.e-6) then
        abgr_remove_fr = (yield + clip) / pl_mass(j)%ab_gr(ipl)%m
      else
        abgr_remove_fr = 1.
      endif 
      if (abgr_remove_fr > 1.0) abgr_remove_fr = 1.0

      !! reset leaf area index and fraction of growing season
      if (pl_mass(j)%tot(ipl)%m > 0.001) then
        pcom(j)%plg(ipl)%lai = pcom(j)%plg(ipl)%lai * (1. - abgr_remove_fr)
        if (pcom(j)%plg(ipl)%lai < pldb(idp)%alai_min) then   !Sue
          pcom(j)%plg(ipl)%lai = pldb(idp)%alai_min
        end if
        pcom(j)%plcur(ipl)%phuacc = pcom(j)%plcur(ipl)%phuacc * (1. - abgr_remove_fr) 
        pcom(j)%plg(ipl)%root_frac = pl_mass(j)%root(ipl)%m / pl_mass(j)%tot(ipl)%m
      else
        pl_mass(j)%tot(ipl)%m = 0.
        pcom(j)%plg(ipl)%lai = 0.
        pcom(j)%plcur(ipl)%phuacc = 0.
      endif

	  !! remove n and p in harvested yield, clipped biomass, and dead roots 
      pl_mass(j)%tot(ipl)%m = pl_mass(j)%tot(ipl)%m - yield
      pl_mass(j)%tot(ipl)%n = pl_mass(j)%tot(ipl)%n - yieldn
      pl_mass(j)%tot(ipl)%p = pl_mass(j)%tot(ipl)%p - yieldp 
      if (pl_mass(j)%tot(ipl)%m < 0.) pl_mass(j)%tot(ipl)%m = 0.
      if (pl_mass(j)%tot(ipl)%n < 0.) pl_mass(j)%tot(ipl)%n = 0.
      if (pl_mass(j)%tot(ipl)%p < 0.) pl_mass(j)%tot(ipl)%p = 0.

	  !! adjust foliar pesticide for plant removal
        do k = 1, cs_db%num_pests
          !! calculate amount of pesticide removed with yield and clippings
          yldpst = 0.
          clippst = 0.
          if (pldb(idp)%hvsti > 1.001) then
            yldpst = cs_pl(j)%pest(k)
            cs_pl(j)%pest(k) = 0.
          else
            yldpst = hiad1 * cs_pl(j)%pest(k)
            cs_pl(j)%pest(k) = cs_pl(j)%pest(k) - yldpst
            if (cs_pl(j)%pest(k) < 0.) cs_pl(j)%pest(k) = 0.
          endif
          clippst = yldpst * (1. - harveff)
          if (clippst < 0.) clippst = 0.
          !! add pesticide in clippings to soil surface
          cs_soil(j)%ly(1)%pest(k) = cs_soil(j)%ly(1)%pest(k) + clippst
        end do   

      return
      end subroutine mgt_harvestop