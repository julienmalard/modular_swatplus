      subroutine mgt_harvgrainop (jj, iplant, iharvop)

!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine performs the harvest grain only operation 

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units          |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    cnyld(:)    |kg N/kg yield  |fraction of nitrogen in yield
!!    cpyld(:)    |kg P/kg yield  |fraction of phosphorus in yield
!!    curyr       |none           |current year in simulation
!!    hvsti(:)    |(kg/ha)/(kg/ha)|harvest index: crop yield/aboveground
!!                                |biomass
!!    ihru        |none           |HRU number
!!    wsyf(:)     |(kg/ha)/(kg/ha)|Value of harvest index between 0 and HVSTI
!!                                |which represents the lowest value expected
!!                                |due to water stress
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    hiad1       |
!!    j           |none          |HRU number
!!    k           |none          |counter
!!    resnew      |
!!    wur         |
!!    yield       |kg            |yield (dry weight)
!!    yieldn      |
!!    yieldp      |
!!    yldpst      |kg pst/ha     |pesticide removed in yield
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Exp, Max, Min
!!    SWAT: curno

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

      use basin_module
      use hru_module, only : ihru, ipl, yield
      use plant_module
      use plant_data_module
      use mgt_operations_module
      use organic_mineral_mass_module
      use carbon_module
      
      implicit none
 
      integer :: j                     !none           |HRU number
      integer :: k                     !none           |counter
      integer :: idp                   !               |
      integer, intent (in) :: jj       !none           |counter
      integer, intent (in) :: iplant   !               |plant number xwalked from hlt_db()%plant and plants.plt
      integer, intent (in) :: iharvop  !               |harvest operation type
      real :: xx                       !varies         |variable to hold calculation results 
      real :: hiad1                    !none           |actual harvest index (adj for water/growth)
      real :: wur                      !none           |water deficiency factor
      real :: resnew                   !               |
      real :: yieldn                   !               |
      real :: yieldp                   !               |
      real :: yldpst                   !kg pst/ha      |pesticide removed in yield
      real :: harveff

      j = jj
      ipl = iplant
      idp = pcom(j)%plcur(ipl)%idplt
      harveff = harvop_db(iharvop)%eff

      if (pcom(j)%plg(ipl)%plpet < 10.) then
        wur = 100.
      else
        wur = 100. * pcom(j)%plg(ipl)%plet / pcom(j)%plg(ipl)%plpet
      endif

      hiad1 = (pcom(j)%plg(ipl)%hvstiadj - pldb(idp)%wsyf) *           &   
            (wur / (wur + Exp(6.13 - .0883 * wur))) + pldb(idp)%wsyf
      if (hiad1 > pldb(idp)%hvsti) hiad1 = pldb(idp)%hvsti

!! check if yield is from above or below ground
      if (pldb(idp)%hvsti > 1.001) then
        yield = pl_mass(j)%tot(ipl)%m * (1. - 1. / (1. + hiad1))
      else
        yield = pl_mass(j)%ab_gr(ipl)%m * hiad1
      endif
       if (yield < 0.) yield = 0.
      yield = yield * harveff

      !!add by zhang
      !!====================
      if (bsn_cc%cswat == 2) then
        cbn_loss(j)%grainc_d = cbn_loss(j)%grainc_d + yield * 0.42
      end if
      !!add by zhang
      !!====================      
      
!! calculate nutrients removed with yield
      yieldn = yield * pldb(idp)%cnyld
      yieldp = yield * pldb(idp)%cpyld
      yieldn = Min(yieldn, 0.85 * pl_mass(j)%tot(ipl)%n)
      yieldp = Min(yieldp, 0.85 * pl_mass(j)%tot(ipl)%p)
      pl_mass(j)%tot(ipl)%n = pl_mass(j)%tot(ipl)%n - yieldn
      pl_mass(j)%tot(ipl)%n = Max(0.,pl_mass(j)%tot(ipl)%n)
      pl_mass(j)%tot(ipl)%p = pl_mass(j)%tot(ipl)%p - yieldp
      pl_mass(j)%tot(ipl)%p = Max(0.,pl_mass(j)%tot(ipl)%p)

!! summary calculations
       xx = pl_mass(j)%tot(ipl)%m
       pl_mass(j)%tot(ipl)%m = pl_mass(j)%tot(ipl)%m - yield
       pl_mass(j)%root(ipl)%m = pl_mass(j)%root(ipl)%m * xx / (pl_mass(j)%tot(ipl)%m + 1.e-6)

      return
      end  subroutine mgt_harvgrainop